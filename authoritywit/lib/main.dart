
import 'package:authoritywit/authoritywit2_widget/authoritywit2_widget.dart';
import 'package:flutter/material.dart';

void main() => runApp(App());


class App extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  
    return MaterialApp(
      home: Authoritywit2Widget(),
    );
  }
}