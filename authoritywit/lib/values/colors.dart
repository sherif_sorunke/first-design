
import 'dart:ui';


class AppColors {
  static const Color primaryBackground = Color.fromARGB(255, 255, 255, 255);
  static const Color primaryElement = Color.fromARGB(255, 255, 255, 255);
  static const Color primaryText = Color.fromARGB(255, 95, 124, 168);
  static const Color secondaryText = Color.fromARGB(255, 0, 0, 0);
  static const Color accentText = Color.fromARGB(255, 114, 100, 92);
}