
import 'package:authoritywit/values/values.dart';
import 'package:flutter/material.dart';


class Authoritywit2Widget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 226, 231, 236),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: 73,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned(
                    left: 0,
                    top: 0,
                    right: 0,
                    child: Image.asset(
                      "assets/images/path-2207.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                    left: 25,
                    top: 31,
                    right: 25,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 20,
                            height: 16,
                            margin: EdgeInsets.only(top: 6),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: 20,
                                    height: 3,
                                    child: Image.asset(
                                      "assets/images/path-36.png",
                                      fit: BoxFit.none,
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: 16,
                                    height: 3,
                                    margin: EdgeInsets.only(top: 3),
                                    child: Image.asset(
                                      "assets/images/path-36-2.png",
                                      fit: BoxFit.none,
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    width: 20,
                                    height: 3,
                                    margin: EdgeInsets.only(top: 4),
                                    child: Image.asset(
                                      "assets/images/path-36.png",
                                      fit: BoxFit.none,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 25,
                            height: 25,
                            margin: EdgeInsets.only(top: 1, right: 35),
                            child: Image.asset(
                              "assets/images/group-953.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 20,
                            height: 25,
                            child: Image.asset(
                              "assets/images/bell.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                margin: EdgeInsets.only(left: 26, top: 18),
                child: Opacity(
                  opacity: 0.58,
                  child: Text(
                    "TO-DO",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: AppColors.secondaryText,
                      fontFamily: "Proxima Nova",
                      fontWeight: FontWeight.w700,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: 118,
              margin: EdgeInsets.only(left: 26, top: 12, right: 25),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned(
                    top: 0,
                    child: Opacity(
                      opacity: 0.99887,
                      child: Container(
                        width: 324,
                        height: 118,
                        decoration: BoxDecoration(
                          color: AppColors.primaryBackground,
                          boxShadow: [
                            Shadows.primaryShadow,
                          ],
                          borderRadius: Radii.k2pxRadius,
                        ),
                        child: Container(),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 0,
                    top: 10,
                    right: 0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Container(
                          height: 17,
                          margin: EdgeInsets.only(left: 12, right: 14),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Opacity(
                                  opacity: 0.58,
                                  child: Text(
                                    "Latest To-Dos",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 242, 90, 3),
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                              Spacer(),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  width: 3,
                                  height: 15,
                                  margin: EdgeInsets.only(top: 2),
                                  child: Image.asset(
                                    "assets/images/group-1012.png",
                                    fit: BoxFit.none,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Opacity(
                            opacity: 0.07233,
                            child: Container(
                              width: 324,
                              height: 4,
                              margin: EdgeInsets.only(top: 8),
                              decoration: BoxDecoration(
                                gradient: Gradients.primaryGradient,
                                boxShadow: [
                                  Shadows.secondaryShadow,
                                ],
                              ),
                              child: Container(),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: 13, top: 28),
                            child: Opacity(
                              opacity: 0.58,
                              child: Text(
                                "No To-dos found",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 118,
              margin: EdgeInsets.only(left: 26, top: 15, right: 25),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned(
                    top: 0,
                    child: Opacity(
                      opacity: 0.99887,
                      child: Container(
                        width: 324,
                        height: 118,
                        decoration: BoxDecoration(
                          color: AppColors.primaryBackground,
                          boxShadow: [
                            Shadows.primaryShadow,
                          ],
                          borderRadius: Radii.k2pxRadius,
                        ),
                        child: Container(),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 0,
                    top: 10,
                    right: 0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Container(
                          height: 17,
                          margin: EdgeInsets.only(left: 12, right: 14),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Align(
                                alignment: Alignment.topLeft,
                                child: Opacity(
                                  opacity: 0.58,
                                  child: Text(
                                    "Latest Finished To-Dos",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 52, 95, 5),
                                      fontFamily: "Proxima Nova",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                              Spacer(),
                              Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                  width: 3,
                                  height: 15,
                                  margin: EdgeInsets.only(top: 1),
                                  child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Positioned(
                                        left: 0,
                                        top: 0,
                                        right: 0,
                                        bottom: 0,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.stretch,
                                          children: [
                                            Container(
                                              height: 3,
                                              child: Image.asset(
                                                "assets/images/image.png",
                                                fit: BoxFit.none,
                                              ),
                                            ),
                                            Spacer(),
                                            Container(
                                              height: 3,
                                              child: Image.asset(
                                                "assets/images/image.png",
                                                fit: BoxFit.none,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Positioned(
                                        left: 0,
                                        right: 0,
                                        child: Image.asset(
                                          "assets/images/image.png",
                                          fit: BoxFit.none,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Opacity(
                            opacity: 0.07233,
                            child: Container(
                              width: 324,
                              height: 4,
                              margin: EdgeInsets.only(top: 8),
                              decoration: BoxDecoration(
                                gradient: Gradients.primaryGradient,
                                boxShadow: [
                                  Shadows.secondaryShadow,
                                ],
                              ),
                              child: Container(),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: 13, top: 28),
                            child: Opacity(
                              opacity: 0.58,
                              child: Text(
                                "No To-dos found",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: AppColors.accentText,
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 20,
              margin: EdgeInsets.only(left: 26, top: 31, right: 25),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Opacity(
                      opacity: 0.58,
                      child: Text(
                        "TASKS",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: AppColors.secondaryText,
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w700,
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ),
                  Spacer(),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: EdgeInsets.only(top: 4),
                      child: Text(
                        "View All Tasks",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Color.fromARGB(255, 3, 169, 244),
                          fontFamily: "Proxima Nova",
                          fontWeight: FontWeight.w400,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 232,
              margin: EdgeInsets.only(left: 26, top: 12, right: 25),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned(
                    top: 0,
                    child: Opacity(
                      opacity: 0.99887,
                      child: Container(
                        width: 324,
                        height: 232,
                        decoration: BoxDecoration(
                          color: AppColors.primaryBackground,
                          boxShadow: [
                            Shadows.primaryShadow,
                          ],
                          borderRadius: Radii.k2pxRadius,
                        ),
                        child: Container(),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 0,
                    top: 0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topCenter,
                          child: Opacity(
                            opacity: 0.37166,
                            child: Container(
                              width: 324,
                              height: 42,
                              decoration: BoxDecoration(
                                color: AppColors.primaryElement,
                                boxShadow: [
                                  Shadows.primaryShadow,
                                ],
                                borderRadius: Radii.k2pxRadius,
                              ),
                              child: Container(),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(left: 18, top: 26),
                            child: Opacity(
                              opacity: 0.58,
                              child: Text(
                                "No entries found",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 119, 113, 109),
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            width: 78,
                            height: 49,
                            margin: EdgeInsets.only(top: 22),
                            child: Image.asset(
                              "assets/images/cloud.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            margin: EdgeInsets.only(top: 7),
                            child: Opacity(
                              opacity: 0.58,
                              child: Text(
                                "Add new entry",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 119, 113, 109),
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: 0,
                    top: 11,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 91,
                            height: 17,
                            margin: EdgeInsets.only(left: 23),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Opacity(
                                    opacity: 0.58,
                                    child: Text(
                                      "#",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        color: AppColors.primaryText,
                                        fontFamily: "Proxima Nova",
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Container(
                                    margin: EdgeInsets.only(left: 43),
                                    child: Opacity(
                                      opacity: 0.58,
                                      child: Text(
                                        "Name",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: AppColors.primaryText,
                                          fontFamily: "Proxima Nova",
                                          fontWeight: FontWeight.w700,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Opacity(
                            opacity: 0.07233,
                            child: Container(
                              width: 324,
                              height: 4,
                              margin: EdgeInsets.only(top: 12),
                              decoration: BoxDecoration(
                                gradient: Gradients.primaryGradient,
                                boxShadow: [
                                  Shadows.secondaryShadow,
                                ],
                              ),
                              child: Container(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: 11,
                    right: 34,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            margin: EdgeInsets.only(right: 32),
                            child: Opacity(
                              opacity: 0.58,
                              child: Text(
                                "Status",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: AppColors.primaryText,
                                  fontFamily: "Proxima Nova",
                                  fontWeight: FontWeight.w700,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Opacity(
                            opacity: 0.58,
                            child: Text(
                              "Start Date",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: AppColors.primaryText,
                                fontFamily: "Proxima Nova",
                                fontWeight: FontWeight.w700,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: 53,
                    top: 0,
                    right: 7,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 2,
                            height: 43,
                            child: Opacity(
                              opacity: 0.2675,
                              child: Image.asset(
                                "assets/images/path-2222.png",
                                fit: BoxFit.none,
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 2,
                            height: 43,
                            margin: EdgeInsets.only(left: 77),
                            child: Opacity(
                              opacity: 0.2675,
                              child: Image.asset(
                                "assets/images/path-2222-2.png",
                                fit: BoxFit.none,
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 18,
                            height: 20,
                            margin: EdgeInsets.only(top: 127),
                            child: Image.asset(
                              "assets/images/group-1003.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                        Spacer(),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 2,
                            height: 43,
                            margin: EdgeInsets.only(right: 89),
                            child: Opacity(
                              opacity: 0.2675,
                              child: Image.asset(
                                "assets/images/path-2222-2.png",
                                fit: BoxFit.none,
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 2,
                            height: 43,
                            margin: EdgeInsets.only(right: 5),
                            child: Opacity(
                              opacity: 0.2675,
                              child: Image.asset(
                                "assets/images/path-2222-2.png",
                                fit: BoxFit.none,
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            width: 8,
                            height: 13,
                            margin: EdgeInsets.only(top: 15),
                            child: Image.asset(
                              "assets/images/group-1010.png",
                              fit: BoxFit.none,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Spacer(),
            Container(
              height: 58,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Container(
                      height: 58,
                      decoration: BoxDecoration(
                        color: AppColors.primaryElement,
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromARGB(47, 0, 0, 0),
                            offset: Offset(-10, -5),
                            blurRadius: 15,
                          ),
                        ],
                      ),
                      child: Container(),
                    ),
                  ),
                  Positioned(
                    left: 38,
                    right: 35,
                    bottom: 8,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Container(
                            width: 28,
                            height: 41,
                            margin: EdgeInsets.only(bottom: 1),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Container(
                                  height: 23,
                                  margin: EdgeInsets.only(left: 2),
                                  child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Positioned(
                                        left: 0,
                                        right: 4,
                                        child: Image.asset(
                                          "assets/images/group-945.png",
                                          fit: BoxFit.none,
                                        ),
                                      ),
                                      Positioned(
                                        left: 5,
                                        top: 0,
                                        right: 0,
                                        child: Image.asset(
                                          "assets/images/group-947.png",
                                          fit: BoxFit.none,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Spacer(),
                                Text(
                                  "Tasks",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 80, 100, 124),
                                    fontFamily: "Segoe UI",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 12,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Container(
                            width: 42,
                            height: 42,
                            margin: EdgeInsets.only(left: 49),
                            child: Stack(
                              alignment: Alignment.bottomCenter,
                              children: [
                                Positioned(
                                  left: 0,
                                  right: 0,
                                  bottom: 0,
                                  child: Opacity(
                                    opacity: 0.58,
                                    child: Text(
                                      "Projects",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 112, 112, 112),
                                        fontFamily: "Segoe UI",
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ),
                                Positioned(
                                  left: 7,
                                  top: 0,
                                  right: 7,
                                  child: Image.asset(
                                    "assets/images/start-up.png",
                                    fit: BoxFit.none,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Container(
                            width: 51,
                            height: 41,
                            margin: EdgeInsets.only(right: 49),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Container(
                                  height: 23,
                                  margin: EdgeInsets.only(left: 13, right: 15),
                                  child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Positioned(
                                        left: 0,
                                        right: 0,
                                        child: Image.asset(
                                          "assets/images/group-949.png",
                                          fit: BoxFit.none,
                                        ),
                                      ),
                                      Positioned(
                                        left: 5,
                                        top: 3,
                                        right: 11,
                                        child: Image.asset(
                                          "assets/images/group-951.png",
                                          fit: BoxFit.none,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Spacer(),
                                Opacity(
                                  opacity: 0.58,
                                  child: Text(
                                    "Reminder",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 112, 112, 112),
                                      fontFamily: "Segoe UI",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Container(
                            width: 35,
                            height: 42,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Container(
                                  height: 23,
                                  margin: EdgeInsets.only(left: 5, right: 7),
                                  child: Image.asset(
                                    "assets/images/user-1.png",
                                    fit: BoxFit.none,
                                  ),
                                ),
                                Spacer(),
                                Opacity(
                                  opacity: 0.58,
                                  child: Text(
                                    "Profile",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: AppColors.secondaryText,
                                      fontFamily: "Segoe UI",
                                      fontWeight: FontWeight.w400,
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}